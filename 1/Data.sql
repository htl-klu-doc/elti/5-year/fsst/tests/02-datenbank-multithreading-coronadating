-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: corona_dating
-- ------------------------------------------------------
-- Server version	5.1.53-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dates`
--

DROP TABLE IF EXISTS `dates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dates` (
  `iddates` int(11) NOT NULL AUTO_INCREMENT,
  `start_time` datetime NOT NULL,
  `end_time` datetime DEFAULT NULL,
  `topic` varchar(45) NOT NULL,
  `hearts` int(11) NOT NULL DEFAULT '0',
  `user_iduser1` int(11) NOT NULL,
  `user_iduser2` int(11) NOT NULL,
  PRIMARY KEY (`iddates`),
  UNIQUE KEY `iddates_UNIQUE` (`iddates`),
  KEY `fk_dates_user_idx` (`user_iduser1`),
  KEY `fk_dates_user1_idx` (`user_iduser2`),
  CONSTRAINT `fk_dates_user` FOREIGN KEY (`user_iduser1`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_dates_user1` FOREIGN KEY (`user_iduser2`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dates`
--

LOCK TABLES `dates` WRITE;
/*!40000 ALTER TABLE `dates` DISABLE KEYS */;
INSERT INTO `dates` VALUES (1,'2022-01-31 08:20:00','2022-01-31 09:07:42','Weather',1,1,2),(2,'2022-01-31 09:30:00','2022-01-31 10:20:00','Corona',2,1,4),(3,'2022-01-31 08:20:00','2022-01-31 09:30:00','GLF',0,2,3),(4,'2022-01-31 09:30:00','2022-01-31 10:20:00','Leben',0,3,1),(5,'2022-01-31 08:20:00','2022-01-31 09:30:00','Divers',2,4,5),(6,'2022-01-31 08:41:31','2022-01-31 08:44:15','Computer',0,1,2),(7,'2022-01-31 08:44:15',NULL,'Computer',0,1,2),(8,'2022-01-31 08:44:59','2022-01-31 08:44:59','Computer',0,1,2),(9,'2022-01-31 08:45:57','2022-01-31 09:45:57','Computer',0,1,2),(10,'2022-01-31 09:01:11','2022-01-31 10:01:11','Computer',0,1,2),(11,'2022-01-31 09:01:28','2022-01-31 10:01:28','Computer',0,1,2),(12,'2022-01-31 09:01:54','2022-01-31 10:01:54','Computer',0,1,2),(13,'2022-01-31 09:02:56','2022-01-31 10:02:56','Computer',0,1,2),(14,'2022-01-31 09:03:07','2022-01-31 10:03:07','Computer',0,1,2),(15,'2022-01-31 09:03:28',NULL,'Maskenball',0,5,1);
/*!40000 ALTER TABLE `dates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `firstname` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  PRIMARY KEY (`iduser`),
  UNIQUE KEY `iduser_UNIQUE` (`iduser`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Franz1948','Franz','Hubert','Zuhause 1'),(2,'Alex3','Alex','Klein','Zuhause 2'),(3,'Franzi','Franzi','Gross','Zuhause 3'),(4,'Anna24','Anna','Klein','Zuhause 4'),(5,'Stefanie','Stefanie','Maier','Zuhause 5');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-31  9:19:04
