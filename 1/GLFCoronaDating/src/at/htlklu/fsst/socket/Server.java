package at.htlklu.fsst.socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import at.htlklu.fsst.helper.Helper;

public class Server extends Thread {
	public static final int PORT = 5710;
	public static boolean SERVER_RUNNING = true;

	private boolean keepRunning = true;
	private PrintWriter out;
	private BufferedReader in;

	public static void main(String[] args) throws IOException {
		ServerSocket serverSocket = new ServerSocket(PORT);

		while (SERVER_RUNNING) {
			System.out.println("Starting new Server instance...");
			Server s = new Server(serverSocket.accept());
			s.start();
		}

		serverSocket.close();
	}

	public Server(Socket s) {
		try {
			in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			out = new PrintWriter(s.getOutputStream(), true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try {
			out.println(Helper.getWelcomeMessage());

			while (keepRunning)
				out.println(communicate(in.readLine()));

			in.close();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String communicate(String input) {
		if (input == null)
			return "Empty input";

		String[] tokens = input.split(" ");
		if (tokens.length == 1 && tokens[0].equals("EXIT")) {
			keepRunning = false;
			return "Bye";
		}

		if (tokens.length != 2)
			return "UNKNOWN COMMAND";

		switch (tokens[0]) {
		case "STARTDATE":
			return Helper.parseStartDate(tokens[1]);
		case "STOPDATE":
			return Helper.parseEndDate(tokens[1]);
		default:
			return "UNKNOWN COMMAND";
		}
	}
}
