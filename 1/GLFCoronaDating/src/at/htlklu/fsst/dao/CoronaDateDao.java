package at.htlklu.fsst.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.Instant;

import at.htlklu.fsst.db.DbConnection;
import at.htlklu.fsst.pojo.CoronaDate;

public class CoronaDateDao {
	public static void main(String[] args) {
		CoronaDate coronaDate = new CoronaDate(Timestamp.from(Instant.now()), "Computer", 1, 2);
		System.out.println(startDate(coronaDate));
		System.out.println(coronaDate.getId());
		System.out.println(endDate(coronaDate.getId(), Timestamp.from(Instant.now().plusSeconds(3600))));
	}

	public static int startDate(CoronaDate coronaDate) {
		try (PreparedStatement pstmt = DbConnection.getConnection().prepareStatement(
				"INSERT INTO dates (start_time, topic, user_iduser1, user_iduser2) VALUES (?, ?, ?, ?)",
				Statement.RETURN_GENERATED_KEYS)) {
			pstmt.setTimestamp(1, coronaDate.getStartTime());
			pstmt.setString(2, coronaDate.getTopic());
			pstmt.setInt(3, coronaDate.getIdUser1());
			pstmt.setInt(4, coronaDate.getIdUser2());

			int value = pstmt.executeUpdate();
			ResultSet rset = pstmt.getGeneratedKeys();
			if (rset.next())
				coronaDate.setId(rset.getInt(1));

			return value;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	public static int endDate(int id, Timestamp endTime) {
		try (PreparedStatement pstmt = DbConnection.getConnection()
				.prepareStatement("UPDATE dates SET end_time = ? WHERE iddates = ?", Statement.RETURN_GENERATED_KEYS)) {
			pstmt.setTimestamp(1, endTime);
			pstmt.setInt(2, id);

			return pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}
}
