package at.htlklu.fsst.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import at.htlklu.fsst.db.DbConnection;

public class UserDao {
	public static void main(String[] args) {
		System.out.println(getFirstName(2));
	}
	
	public static String getFirstName(int id) {
		try (PreparedStatement pstmt = DbConnection.getConnection()
				.prepareStatement("SELECT firstname FROM user WHERE iduser = ?")) {
			pstmt.setInt(1, id);
			ResultSet rset = pstmt.executeQuery();

			if (rset.next())
				return rset.getString(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
