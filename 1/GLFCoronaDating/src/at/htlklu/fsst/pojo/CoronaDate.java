package at.htlklu.fsst.pojo;

import java.sql.Timestamp;

public class CoronaDate {
	private int id;
	private Timestamp startTime;
	private Timestamp endTime;
	private String topic;
	private int hearts;
	private int idUser1;
	private int idUser2;

	public CoronaDate(int id, Timestamp startTime, Timestamp endTime, String topic, int hearts, int idUser1,
			int idUser2) {
		this.id = id;
		this.startTime = startTime;
		this.endTime = endTime;
		this.topic = topic;
		this.hearts = hearts;
		this.idUser1 = idUser1;
		this.idUser2 = idUser2;
	}

	public CoronaDate(Timestamp startTime, String topic, int idUser1, int idUser2) {
		this.startTime = startTime;
		this.topic = topic;
		this.idUser1 = idUser1;
		this.idUser2 = idUser2;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getStartTime() {
		return startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	public Timestamp getEndTime() {
		return endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public int getHearts() {
		return hearts;
	}

	public void setHearts(int hearts) {
		this.hearts = hearts;
	}

	public int getIdUser1() {
		return idUser1;
	}

	public void setIdUser1(int idUser1) {
		this.idUser1 = idUser1;
	}

	public int getIdUser2() {
		return idUser2;
	}

	public void setIdUser2(int idUser2) {
		this.idUser2 = idUser2;
	}
	
	

}
