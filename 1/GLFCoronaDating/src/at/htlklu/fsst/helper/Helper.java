package at.htlklu.fsst.helper;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import at.htlklu.fsst.dao.CoronaDateDao;
import at.htlklu.fsst.dao.UserDao;
import at.htlklu.fsst.pojo.CoronaDate;

public class Helper {

	public static String parseStartDate(String data) {
		String[] tokens = data.split("_");

		if (tokens.length != 3)
			return "Invalid format! Please use 'STARTDATE <dating_topic>_<partner_id_1>_<partner_id_2>";

		int id1 = 0;
		int id2 = 0;

		try {
			id1 = Integer.parseInt(tokens[1]);
			id2 = Integer.parseInt(tokens[2]);
		} catch (NumberFormatException e) {
			return "Please use a valid id!";
		}

		CoronaDate coronaDate = new CoronaDate(Timestamp.from(Instant.now()), tokens[0], id1, id2);
		if (CoronaDateDao.startDate(coronaDate) != 1)
			return "Could not add Date to Database!";

		String firstName1 = UserDao.getFirstName(id1);
		String firstName2 = UserDao.getFirstName(id2);

		return String.format("Hallo %s und %s, eure Dating-ID ist %d", firstName1, firstName2, coronaDate.getId());
	}

	public static String parseEndDate(String data) {
		int id = 0;

		try {
			id = Integer.parseInt(data);
		} catch (NumberFormatException e) {
			return "Please use a valid id!";
		}

		if (CoronaDateDao.endDate(id, Timestamp.from(Instant.now())) != 1)
			return String.format("DATE %d DID NOT YET TAKE PLACE", id);
		
		
		return String.format("Ok, Date mit der ID %d ist beendet", id);
	}

	public static String getWelcomeMessage() {
		return String.format("Willkommen zum Corona-Dating-Event im %s", getCurrentDateYear());
	}

	private static String getCurrentDateYear() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MMMM y");
		return dtf.format(LocalDateTime.now());
	}
}
